/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package combsort;

/**
 *
 * @author edwinfinch
 */
public class CombSort {

    //http://stackoverflow.com/questions/2808535/round-a-double-to-2-decimal-places
    public static double round(double value, int places) {
        if (places < 0){
            throw new IllegalArgumentException();
        }

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);

        return (double) tmp / factor;
    }

    public static int indexOf(int[] arrayToSearch, int toFind) {
        int low = 0;
        int high = arrayToSearch.length - 1;
        while (low <= high) {
            int middle = low + (high - low) / 2;
            if(toFind < arrayToSearch[middle]){
                high = middle - 1;
            }
            else if(toFind > arrayToSearch[middle]){
                low = middle + 1;
            }
            else{
                return middle;
            }
        }
        return -1;
    }

    public static int[] combSort(int[] toSort){
        long startTime = System.currentTimeMillis() % 1000;
        long swapCount = 0;

        //System.out.println("Starting sort...");

        double gap = toSort.length;

        //System.out.println("Combsorting an array with length " + gap);

        boolean swapped = true;
        while(gap > 1 || swapped){
            //System.out.println("Running, swapped is false. Current gap is " + gap);

            if (gap > 1) {
                gap = (gap / 1.3);
            }

            swapped = false;

            int lastValue = 0;
            for (int i = 0; (i + gap) < toSort.length; i++) {
                int current = toSort[i];
                int next = toSort[i+(int)gap];
                //System.out.println("Current value " + current + " and next " + next);
                if(current > next){
                    //System.out.println("Swapping values.");
                    toSort[i] = next;
                    toSort[i+(int)gap] = current;
                    swapped = true;
                    swapCount++;
                }

                if(current == next){
                    //System.out.println("Values are already equal.");
                    //swapped = true; //causes an infinite loop, oops
                }

                lastValue = i;
            }
            //System.out.println("Finished with i+gap of " + (lastValue+gap));
        }
        long endTime = System.currentTimeMillis() % 1000;

        long completedTime = (endTime-startTime);
        if(completedTime > 0){
            System.out.println("Completed sort in " + completedTime + "ms. That's about " + (toSort.length/completedTime) + " items/ms.");
        }
        else{
            System.out.println("Sorted " + toSort.length + " items in less than 1ms. Vytas boast.");
        }

        System.out.println("Total swap count: " + swapCount + ". That's about " + round((((double)swapCount/(double)toSort.length)*100), 2) + "% of length of the array that was sorted.");

        return toSort;
    }

    /**
     * @param args the command line arguments
     */
    public static void printArray(int[] array){
        String arrayString = "Array " + array.toString() + " of length " + array.length + ":\n";
        for(int item : array){
            arrayString += item + " ";
        }
        System.out.println(arrayString);
    }

    public static void main(String[] args) {
        if(args.length < 1){
            int amount = (int)(Math.random() * 500);
            //System.out.println("Sorting for " + amount + " items.");
            int[] randomArray = new int[amount];
            for(int i = 0; i < randomArray.length; i++){
                randomArray[i] = (int)(Math.random() * 10000);
            }

            printArray(randomArray);
            printArray(combSort(randomArray));
        }
        else{
            try{
                int[] parsedNumbers = new int[args.length];
                boolean hasFind = false;
                if(args[args.length-1].contains("find")){
                    hasFind = true;
                }
                for(int i = 0; i < (hasFind ? args.length-1 : args.length); i++){
                    //System.out.println(args[i]);
                    parsedNumbers[i] = Integer.parseInt(args[i]);
                }
                printArray(parsedNumbers);
                parsedNumbers = combSort(parsedNumbers);
                printArray(parsedNumbers);

                if(hasFind){
                    int toFindNumber = Integer.parseInt(args[args.length-1].substring("find".length()));
                    int foundIndex = indexOf(parsedNumbers, toFindNumber);
                    if(foundIndex > -1){
                        System.out.println("Found number " + toFindNumber + " at index " + foundIndex);
                    }
                    else{
                        System.out.println("Didn't find number " + toFindNumber + ", sadly.");
                    }
                }
            }
            catch(Exception e){
                e.printStackTrace();
                //Windows error
            }
        }
    }

}
