var mongojs = require('mongojs');
var db = mongojs('telegram_bot_analytics', ['group_chat_stats', 'group_chat_messages']);

var EDWIN_ID = 1494633;

module.exports = {
    writeToDatabase: function (stats, toAdd, callback) {
        db.group_chat_messages.insert(toAdd, function(error, docs){
            if(callback){
                callback(error, docs);
            }
        });
    },
    readFromDatabase: function (stats, toFind, callback) {
        db.group_chat_messages.find(toFind, function(err, docs){
            if(callback){
                callback(err, docs);
            }
        });
    },
    getTLDRData: function(totalMessages, howFarBack, callback){
        db.group_chat_messages.find({}).skip(totalMessages - howFarBack, function(err, docs){
            if(callback){
                callback(err, docs);
            }
        });
    },
    getUserStats: function(userID, callback){
        db.group_chat_messages.count({"message.from.id":userID, "message.date":{"$gt":1451624400}}, function(err, count){
            if(err){
                console.log("Error getting stats: " + err);
                callback(undefined);
                return;
            }
            db.group_chat_messages.count({"message.from.id":EDWIN_ID, "message.date":{"$gt":1451624400}}, function(edwinError, edwinCount){
                if(edwinError){
                    console.log("Error getting stats: " + edwinError);
                    callback(undefined);
                    return;
                }
                var stats = {
                    sent: count,
                    edwinSent: edwinCount
                };
                callback(stats);
            });
        });
    },
    getTotalCount: function(callback){
        db.group_chat_messages.count({}, function(err, count){
            if(err){
                console.log("Error " + err);
                callback(undefined);
            }
            callback(count);
        });
    },
    getTotalCountSinceNewYear: function(callback){
        db.group_chat_messages.count({"message.date":{"$gt":1451624400}}, function(err, count){
            if(err){
                console.log("Error " + err);
                callback(undefined);
                return;
            }
            callback(count);
        });
    },
    getCountForSearch: function(toFind, toSave, callback){
        var toSave = toSave;
        db.group_chat_messages.count(toFind, function(err, count){
            if(err){
                console.log("Error getting count: " + err);
                callback(undefined);
                return;
            }
            var stats = {
                count: count,
                saved: toSave,
                found: toFind
            };
            callback(stats);
        });
    }
};
