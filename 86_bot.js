var TelegramBot = require('node-telegram-bot-api');

var token = '198164412:AAHBjbH823mQzQD6xaybUKemUZuyQEPsOJ8';
var tg = require('telegram-cli')('tg/bin/telegram-cli', 'tg/tg-server.pub');
var mongojs = require('mongojs');
var db = mongojs('telegram_bot_analytics', ['data_86']);

var bot = new TelegramBot(token, {polling: true});

// Matches /echo [whatever]
bot.onText(/\/echo (.+)/, function (msg, match) {
    var fromId = msg.chat.id;
    var resp = match[1];
    bot.sendMessage(fromId, resp);
});

tg.on('ready', function() {
    console.log("READY");
    bot.sendMessage(1494633, "I'm alive");
});

bot.on("new_chat_participant", function(msg, match){
    console.log("New peep " + JSON.stringify(msg));
    if(msg.new_chat_participant.username === "The86Bot"){
        bot.sendMessage(msg.chat.id, "The future of chat moderation has arrived, suckers");
        return;
    }
    isBanned(msg.new_chat_participant.id, function(reasonBanned){
        if(reasonBanned){
            bot.sendMessage(msg.chat.id, "Sorry sucker, you're banned.");
            kick(msg.message_id, msg.chat.id, msg.new_chat_participant.id);
        }
    });
});

bot.onText(/\/test/, function(msg, match){
    tg.msg('Edwin', "Test message");
});

function contains(text, string){
    var value = text.indexOf(string) > -1;
    return value;
}

var admins = [];
function refuse(msg){
    console.log(JSON.stringify(admins));
    console.log("message " + JSON.stringify(msg));
    if(msg.reply_to_message){
        var isKick = (contains(msg.text, "kick") || contains(msg.text, "ban"));
        if((admins.indexOf(msg.reply_to_message.from.id) > -1) && isKick){
            bot.sendMessage(msg.chat.id, "You can't kick a fellow admin, it'd be like me hating Green Day");
            return true;
        }
        else if(msg.reply_to_message.from.username === "Lignite" && isKick){
            bot.sendMessage(msg.chat.id, "You can't kick 86 himself");
            return true;
        }
    }
    if(msg.from.username === "Lignite"){
        return false;
    }
    if(admins.indexOf(msg.from.id) > -1){
        return false;
    }
    return true;
}

function isBanned(userID, callback){
    db.data_86.find({ userID: userID }, function(error, docs){
        if(error){
            bot.sendMessage(msg.chat.id, "There was an error checking on that person: " + error);
            return;
        }
        if(docs.length > 0){
            callback(docs[0].reason);
        }
        else{
            callback();
        }
    });
}

function kick(message_id, chat_id, user_to_ban_id, reason, ban){
    console.log("kicking " + user_to_ban_id + " banned? " + ban);

    if(user_to_ban_id === 198164412){
        console.log('tried');
        bot.sendMessage(chat_id, "You can't kick 86 himself silly goose", {reply_to_message_id:message_id});
        return;
    }

    if(reason){
        var replyTo = { reply_to_message_id:message_id };
        bot.sendMessage(chat_id, "You've been " + (ban ? "banned" : "kicked") + " for " + reason + ", see you later (even though it says Edwin kicked you, it was a bot controlling his account lol)", replyTo);
    }

    if(ban){
        var toBan = {
            userID: user_to_ban_id,
            reason: reason
        };
        isBanned(user_to_ban_id, function(reasonBanned){
            if(reasonBanned){
                console.log("This user was already banned");
            }
            else{
                db.data_86.insert(toBan, function(error, docs){
                    if(error){
                        bot.sendMessage(chat_id, "There was an error banning that person: " + error);
                        return;
                    }
                });
            }
        });
    }

    setTimeout(function(){
        tg.kick(Math.abs(chat_id), user_to_ban_id);
    }, 1500);
}

bot.onText(/\/adminstatus/, function(msg, match){
    if(refuse(msg)){ return; }

    if(msg.text.length > "/adminstatus ".length){
        return;
    }

    bot.sendMessage(msg.chat.id, "Yeah, you have admin status", { reply_to_message_id:msg.message_id })
});

bot.onText(/\/adminstatus (.+)/, function(msg, match){
    if(msg.from.username !== "Lignite"){ return; }

    var adminStatus = match[1] === "on";
    var toGiveID = msg.reply_to_message.from.id;
    if(adminStatus){
        admins.push(toGiveID);
        bot.sendMessage(msg.chat.id, "They have admin status for 86 now");
    } else {
        var index = admins.indexOf(toGiveID);
        if(index > -1){
            admins.splice(index, 1);
            bot.sendMessage(msg.chat.id, "They no longer have admin status for 86");
        }
        else{
            bot.sendMessage(msg.chat.id, "That person never had admin status");
        }
    }
});

bot.onText(/\/unban/, function(msg, match){
    if(refuse(msg)){ return; }

    db.data_86.remove({ userID: msg.reply_to_message.from.id }, function(error, docs){
        if(error){
            bot.sendMessage(msg.chat.id, "There was an error banning that person: " + error);
            return;
        }
        bot.sendMessage(msg.chat.id, "They've been unbanned");
    });
});

bot.onText(/\/ban/, function(msg, match){
    if(refuse(msg)){ return; }

    if(msg.text.length < 5){
        var replyTo = msg.reply_to_message;
        kick(replyTo.message_id, msg.chat.id, replyTo.from.id, "no reason", true);
    }
});

bot.onText(/\/ban (.+)/, function(msg, match){
    if(refuse(msg)){ return; }

    var replyTo = msg.reply_to_message;
    kick(replyTo.message_id, msg.chat.id, replyTo.from.id, match[1], true);
});

bot.onText(/\/kick/, function(msg, match){
    if(refuse(msg)){ return; }

    if(msg.text.length < 7){
        var replyTo = msg.reply_to_message;
        kick(replyTo.message_id, msg.chat.id, replyTo.from.id, "no reason");
    }
});

bot.onText(/\/kick (.+)/, function(msg, match){
    if(refuse(msg)){ return; }

    var replyTo = msg.reply_to_message;
    kick(replyTo.message_id, msg.chat.id, replyTo.from.id, match[1]);
});

bot.onText(/\/off/, function(msg, match){
    if(refuse(msg)){ return; }

    console.log("off");
    tg.close();
    bot.sendMessage(msg.chat.id, "Shut down Telegram-CLI connection.");
});

bot.onText(/\/on/, function(msg, match){
    if(refuse(msg)){ return; }

    tg = require('telegram-cli')('tg/bin/telegram-cli', 'tg/tg-server.pub');
    bot.sendMessage(msg.chat.id, "Powered on Telegram-CLI connection.");
});

bot.onText(/\/info/, function(msg, match){
    if(refuse(msg)){ return; }

    bot.sendMessage(msg.chat.id, JSON.stringify(msg.reply_to_message), {reply_to_message_id:msg.message_id});
});

bot.onText(/\/groupchat (.+)/, function(msg, match){
    if(refuse(msg)){ return; }

    bot.sendMessage(-12880731, match[1]);
});