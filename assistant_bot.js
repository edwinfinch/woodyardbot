var TelegramBot = require('node-telegram-bot-api');
var PRODUCTION = true;

var MAFIA_GANGS_ID = PRODUCTION ? 231318849 : 1494633;
var INTERVAL = PRODUCTION ? 1000*60*60 : 5000;
var running = false;

var token = '207642678:AAGo2UhO7dyZ_q8EhAnR_VWENMBFfiFxogo';
var tg = require('telegram-cli')('tg/bin/telegram-cli', 'tg/tg-server.pub');

var bot = new TelegramBot(token, {polling: true});

function contains(text, string){ return text.indexOf(string) > -1; }
function refuse(msg){ return msg.from.username !== "Lignite"; }

var taskQuene = [];
var allTasks = ["collect", "buy_h", "mission_9", "levelup"];

function fire(){
    tg = require('telegram-cli')('tg/bin/telegram-cli', 'tg/tg-server.pub');
    tg.on('ready', function() {
        console.log("Telegram-CLI ready. Firing task quene in 5 seconds...");
        setTimeout(fireTaskQuene, 5000);
    });
}

var setup = {
    fire: function(){
        setInterval(function(){
            fire();
        },  (PRODUCTION ? (INTERVAL*0.05) : 5000));
        console.log("Setup main fire.");
    },
    collect: function(){
        setInterval(function(){
            taskQuene.push("collect");
            console.log("Pushed collect onto stack " + Date.now());
        }, INTERVAL*0.25);
        console.log("Setup collect.");
    },
    buy_h: function(){
        setInterval(function(){
            taskQuene.push("buy_h");
            console.log("Pushed buy_h onto stack " + Date.now());
        }, INTERVAL*0.1);
        console.log("Setup buy_h.");
    },
    mission_9: function(){
        setInterval(function(){
            taskQuene.push("mission_9");
            console.log("Pushed mission_9 onto stack " + Date.now());
        }, INTERVAL*0.80);
        console.log("Setup mission_9.");
    },
    levelup: function(){
        setInterval(function(){
            taskQuene.push("levelup");
            console.log("Pushed levelup onto stack " + Date.now());
        }, INTERVAL*6);
        console.log("Setup levelup.");
    }
};

var tasks = {
    collect: function(){
        console.log("Sending collect");
        tg.msg(MAFIA_GANGS_ID, "/collect");
    },
    buy_h: function(){
        console.log("Sending buy_h");
        tg.msg(MAFIA_GANGS_ID, "/buy_h");
    },
    mission_9: function(){
        console.log("Sending mission_9");
        tg.msg(MAFIA_GANGS_ID, "/mission_9");
    },
    levelup: function(){
        console.log("Sending levelup");
        tg.msg(MAFIA_GANGS_ID, "/levelup");
        setTimeout(function(){
            tg.msg(MAFIA_GANGS_ID, "Energy (+1)");
        }, 500);
    }
};

function fireTaskQuene(){
    running = true;
    console.log(taskQuene);
    for(var i = 0; i < taskQuene.length; i++){
        (function(index){
            setTimeout(function(){
                console.log(taskQuene[index]);
                tasks[taskQuene[index]]();
            }, index * 1000)
        })(i);
    }
    setTimeout(function(){
        console.log("Shutting down.");
        tg.close();
        running = false;
        taskQuene = [];
        console.log("Cleared task quene.");
    }, (taskQuene.length * 1000 * 2) + 3000);
}

// Matches /echo [whatever]
bot.onText(/\/echo (.+)/, function (msg, match) {
    var fromId = msg.chat.id;
    var resp = match[1];
    bot.sendMessage(msg.chat.id, match[1]);
});

bot.onText(/\/average/, function(msg, match){
    if(refuse(msg)){ return; }

    bot.sendMessage(msg.chat.id, "Yeah I am");
});

bot.onText(/\/fireall/, function(msg, match){
    if(refuse(msg)){ return; }

    taskQuene = [];
    for(var i = 0; i < allTasks.length; i++){
        taskQuene.push(allTasks[i]);
    }
    setTimeout(fire, 3000);
    bot.sendMessage(msg.chat.id, "Firing quene in 3 seconds: " + taskQuene);
});

bot.onText(/\/taskquene/, function(msg, match){
    if(refuse(msg)){ return; }

    if(taskQuene.length < 1){
        bot.sendMessage(msg.chat.id, "The task quene is currently empty.");
    }
    else{
        bot.sendMessage(msg.chat.id, "The current task quene is: " + taskQuene);
    }
});

bot.onText(/\/running/, function(msg, match){
    if(refuse(msg)){ return; }

    bot.sendMessage(msg.chat.id, running ? "Yeah, I'm running" : "No, I'm not running");
});

bot.on('message', function(msg, match){
    console.log(JSON.stringify(msg));
});

bot.getMe().then(function(me) {
    tg.on('ready', function() {
        console.log("Closing.");
        tg.close();
    });
    var all = ["fire", "collect", "buy_h", "mission_9", "levelup"];
    for(var i = 0; i < all.length; i++){
        setup[all[i]]();
    }
    console.log("Events lined up.");
});

bot.onText(/\/off/, function(msg, match){
    if(refuse(msg)){ return; }

    console.log("off");
    tg.close();
    bot.sendMessage(msg.chat.id, "Manually shut down Telegram-CLI connection.");
});

bot.onText(/\/on/, function(msg, match){
    if(refuse(msg)){ return; }

    tg = require('telegram-cli')('tg/bin/telegram-cli', 'tg/tg-server.pub');
    bot.sendMessage(msg.chat.id, "Powered on Telegram-CLI connection.");
});