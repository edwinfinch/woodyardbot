var request = require('request');
var analytics = require('./chat_analytics');
var fs = require('fs');
var plotly = require('plotly')('edwinfinch','zjoygft1ot');

var download = function(uri, filename, callback){
  request.head(uri, function(err, res, body){
    //console.log('content-type:', res.headers['content-type']);
    //console.log('content-length:', res.headers['content-length']);
    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};

var sendGraph = function(bot, msg, reply){
    var downloadURL = reply.url + "/plot-title.png";
    download(downloadURL, 'image.png', function(){
        bot.sendPhoto(msg.chat.id, 'image.png');
    });
}

var renderGroupChatNames = function(namesArray, amountArray, callback){
    var trace1 = {
        x: namesArray,
        y: amountArray,
        name: "Sent with Name",
        type: "bar"
    };
    var data = [trace1];
    /*
    console.log("Rendering chat names " + JSON.stringify(data));
    callback();
    return;
*/
    var layout = {
        title: "Group Chat Names",
        font:{
            family: "Courier New, monospace",
            size: 24,
            color: "#7f7f7f"
        },
        xaxis: {
            title: "Chat Name",
            titlefont: {
                family: "Courier New, monospace",
                size: 22,
                color: "#7f7f7f"
            },
            tickfont: {
              size: 8,
              color: "#7f7f7f"
            }
        },
        yaxis: {
            title: "Amount Sent",
            titlefont: {
                family: "Courier New, monospace",
                size: 22,
                color: "#7f7f7f"
            }
        }
    };
    console.log(JSON.stringify(layout));

    var graphOptions = {layout: layout, filename: "group-chat-name", fileopt: "overwrite"};
    plotly.plot(data, graphOptions, function (err, reply) {
        if(err){
            console.log(err);
            callback(err);
            return;
        }
        var downloadURL = reply.url + "/plot-title.png";
        console.log("Downloading from " + downloadURL);
        download(downloadURL, 'graphs/group_chat_names.png', function(){
            console.log("Downloaded new group chat name graph.");
            if(callback){
                callback(undefined);
            }
        });
    });
}

var renderGroupChatMembers = function(namesArray, amountArray, callback){
    var trace1 = {
        x: namesArray,
        y: amountArray,
        name: "Sent",
        type: "bar"
    };
    var data = [trace1];
    /*
    console.log("Rendering chat names " + JSON.stringify(data));
    callback();
    return;
*/
    var layout = {
        title: "Group Chat Members",
        font:{
            family: "Courier New, monospace",
            size: 24,
            color: "#7f7f7f"
        },
        xaxis: {
            title: "Name",
            titlefont: {
                family: "Courier New, monospace",
                size: 22,
                color: "#7f7f7f"
            },
            tickfont: {
              size: 8,
              color: "#7f7f7f"
            }
        },
        yaxis: {
            title: "Amount Sent",
            titlefont: {
                family: "Courier New, monospace",
                size: 22,
                color: "#7f7f7f"
            }
        }
    };
    console.log(JSON.stringify(layout));

    var graphOptions = {layout: layout, filename: "group-chat-member", fileopt: "overwrite"};
    plotly.plot(data, graphOptions, function (err, reply) {
        if(err){
            console.log(err);
            callback(err);
            return;
        }
        var downloadURL = reply.url + "/plot-title.png";
        console.log("Downloading from " + downloadURL);
        download(downloadURL, 'graphs/group_chat_members.png', function(){
            console.log("Downloaded new group chat member graph.");
            if(callback){
                callback(undefined);
            }
        });
    });
}

var renderHours = function(date, hoursArray, itemsArray, callback){
    var trace1 = {
        x: hoursArray,
        y: itemsArray,
        name: "Sent",
        type: "scatter"
    };
/*
    console.log("Rendering hours " + JSON.stringify(trace1));
    callback();
    return;
*/
    var data = [trace1];
    var layout = {
        title: "Group Chat Messages on " + (date.getMonth()+1) + "/" + date.getDate() + "/" + date.getFullYear(),
        font:{
            family: "Courier New, monospace",
            size: 20,
            color: "#7f7f7f"
        },
        xaxis: {
            title: "Hour",
            titlefont: {
                family: "Courier New, monospace",
                size: 22,
                color: "#7f7f7f"
            }
        },
        yaxis: {
            title: "Amount Sent",
            titlefont: {
                family: "Courier New, monospace",
                size: 22,
                color: "#7f7f7f"
            }
        }
    };
    var graphOptions = {layout: layout, filename: "per-hour-graph", fileopt: "overwrite"};
    plotly.plot(data, graphOptions, function (err, reply) {
        if(err){
            console.log(err);
            callback(err);
            return;
        }
        var downloadURL = reply.url + "/plot-title.png";
        download(downloadURL, 'graphs/hours.png', function(){
            console.log("Downloaded new hours graph.");
            if(callback){
                callback(undefined);
            }
        });
    });
}

var renderDays = function(date, data, callback){
    var daysArray = data.days;
    var itemsArray = data.items;
    var weekBeforeItemsArray = data.weekBeforeItems;

    var trace1 = {
        x: daysArray,
        y: itemsArray,
        name: "This Week",
        type: "scatter"
    };
    var trace2 = {
        x: daysArray,
        y: weekBeforeItemsArray,
        name: "Last Week",
        type: "scatter"
    };
/*
    console.log("Rendering days " + JSON.stringify(trace1));
    callback();
    return;
*/
    var data = [trace1, trace2];
    var date1 = new Date(date.getTime()-((86400*1000)*7));
    var layout = {
        title: "Group Chat from " + (date1.getMonth()+1) + "/" + (date1.getDate()+1) + "/" + date1.getFullYear() + " to " + (date.getMonth()+1) + "/" + date.getDate() + "/" + date.getFullYear(),
        font:{
            family: "Courier New, monospace",
            size: 20,
            color: "#7f7f7f"
        },
        xaxis: {
            title: "Day",
            titlefont: {
                family: "Courier New, monospace",
                size: 22,
                color: "#7f7f7f"
            }
        },
        yaxis: {
            title: "Amount Sent",
            titlefont: {
                family: "Courier New, monospace",
                size: 22,
                color: "#7f7f7f"
            }
        }
    };
    var graphOptions = {layout: layout, filename: "per-day-graph", fileopt: "overwrite"};
    plotly.plot(data, graphOptions, function (err, reply) {
        if(err){
            console.log(err);
            callback(err);
            return;
        }
        var downloadURL = reply.url + "/plot-title.png";
        console.log("Downloading from " + downloadURL);
        download(downloadURL, 'graphs/days.png', function(){
            console.log("Downloaded new days graph.");
            if(callback){
                callback(undefined);
            }
        });
    });
}

function getFormattedDate(date){
    var dateString = "";
    dateString += date.getFullYear() + "-";
    dateString += (date.getMonth()+1) + "-";
    dateString += date.getDate();

    return dateString;
}

module.exports = {
    renderHoursChart: function(callback){
        var currentTime = Math.floor(Date.now()/1000);
        var date = new Date(currentTime*1000);

        var hours = date.getHours()+1;
        var itemsArray = new Array(hours);
        var hoursArray = new Array(hours);
        console.log(hours);
        var timeOffsetFromNow = 0;
        var lessThanTimeOffset = 0;
        for(var i = 0; i < hours; i++){
            var greaterThan = currentTime-(3600*i)-timeOffsetFromNow;
            var lessThan = currentTime-(3600*i)+lessThanTimeOffset;
            if(i === 0){
                timeOffsetFromNow = date.getSeconds();
                timeOffsetFromNow += date.getMinutes()*60;
                lessThanTimeOffset = 3600-timeOffsetFromNow;
                greaterThan = currentTime-timeOffsetFromNow;
            }
            //console.log("Greater than for hours " + greaterThan + " less than " + lessThan + " difference " + (lessThan-greaterThan) + " for " + (hours-i));
            var toSave = {
                location: i,
                gt: greaterThan,
                lt: lessThan
            };
            analytics.getCountForSearch({ "message.date":{"$gt":greaterThan, "$lt":lessThan} }, toSave, function(data){
                console.log("Got " + JSON.stringify(data) + " for hours");
                itemsArray[data.saved.location] = data.count;
                hoursArray[data.saved.location] = hours-data.saved.location-1;
            });
        }
        setTimeout(function(){
            var data = {
                items: itemsArray,
                hours: hoursArray
            };
            console.log("Items " + JSON.stringify(itemsArray) + " hours " + JSON.stringify(hoursArray));
            //callback();
            renderHours(date, hoursArray, itemsArray, callback);
        }, 5000);
    },
    //week: 604800 seconds
    renderDaysChart: function(callback){
        var currentTime = Math.floor(Date.now()/1000);
        var date = new Date(currentTime*1000);

        var daysBack = 7;
        var weekBeforeItemsArray = new Array(daysBack);
        var itemsArray = new Array(daysBack);
        var daysArray = new Array(daysBack);
        var timeOffsetFromNow = 0;
        var lessThanTimeOffset = 0;
        for(var i = 0; i < daysBack; i++){
            var greaterThan = currentTime-timeOffsetFromNow-(86400*i);
            var lessThan = currentTime-(86400*i)+lessThanTimeOffset;
            if(i === 0){
                timeOffsetFromNow = date.getSeconds();
                timeOffsetFromNow += date.getMinutes()*60;
                timeOffsetFromNow += date.getHours()*60*60;
                lessThanTimeOffset = 86400-timeOffsetFromNow;
                greaterThan = currentTime-timeOffsetFromNow;
            }
            //console.log("Greater than for days " + greaterThan + " less than " + lessThan + " difference " + (lessThan-greaterThan) + " for " + i);
            //console.log("Items " + itemsArray + " days " + daysArray);
            var toSave = {
                location: i,
                gt: greaterThan,
                lt: lessThan
            };
            analytics.getCountForSearch({ "message.date":{"$gt":greaterThan, "$lt":lessThan} }, toSave, function(data){
                itemsArray[data.saved.location] = data.count;
                daysArray[data.saved.location] = getFormattedDate(new Date(date.getTime()-((86400*1000)*data.saved.location)));

                (function(gt, lt, location){
                    console.log("Searching for " + data.saved.location + " gt " + gt + " and lt " + lt);
                    analytics.getCountForSearch({ "message.date":{"$gt":gt-604800, "$lt":lt-604800} }, location, function(weekBeforeData){
                        console.log("Got " + JSON.stringify(weekBeforeData));
                        weekBeforeItemsArray[weekBeforeData.saved] = weekBeforeData.count;
                    });
                })(data.saved.gt, data.saved.lt, data.saved.location);
            });
        }
        setTimeout(function(){
            //daysArray = daysArray.sort();
            var data = {
                weekBeforeItems: weekBeforeItemsArray,
                items: itemsArray,
                days: daysArray
            };
            console.log("Daily " + JSON.stringify(data));
            //callback();
            renderDays(date, data, callback);
        }, 7500);
    },
    renderGroupChatNamesChart: renderGroupChatNames,
    renderGroupChatMembersChart: renderGroupChatMembers
};
