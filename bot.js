/*
Gene Bot
@TheGeneBot

Created by Edwin Finch
*/

var args = process.argv.slice(2);
console.log("Arguments " + args);

var FIX = (args.indexOf("--f") !== -1);

var PRODUCTION = (args.indexOf("--s") === -1);
var LOGGING = true;
var FORCE_GRAPH_RELOAD = false;
var DISABLE_REFUSE = false;
var GROUP_CHAT_ID;
var BOT_USERNAME;
var INVITE_LINK = "https://telegram.me/joinchat/ABbOaQDEi1tGvqXGjcCwgg";
var BEEHIVE_MODE = false;

var CREIGHTON_CREW = -14046499;
var EDWIN_ID = 1494633;
var LUKA_ID = 86527390;

//var WILL_DENNIS = 141877975;
var WILL_DENNIS = 0;

var TelegramBot = require('node-telegram-bot-api');
var fs = require('fs');
var plotly = require('plotly')('edwinfinch', 'zjoygft1ot');
var urban = require('urban');
var analytics = require('./chat_analytics');
var charts = require('./charts');
var request = require('request');

function NSLog(toLog) {
    if (LOGGING) {
        console.log(toLog);
    }
}

//yolo
String.prototype.contains = function(it) {
    return this.indexOf(it) != -1;
};
var replaceAll = function(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function capitalizeFirstLetter(original) {
    if (original == undefined || original.length === 0) {
        return original;
    }
    return original.substring(0, 1).toUpperCase() + original.substring(1);
}

var token;
if (PRODUCTION) {
    //@TheGeneBot
    NSLog("You are running a production bot!");
    token = '183456810:AAFiQIdFBlh1cCUR4MfWG7HGPZ_E7YZF2Bg';
    NSLog(token);
    GROUP_CHAT_ID = -12880731;
    BOT_USERNAME = "TheGeneBot";
} else {
    //@WoodyardSandboxBot
    NSLog("You are running a SANDBOX bot!");
    token = "159148541:AAFXNXFUIZpOsce-zA-ZrZ0CRSsqH6naF1E";
    NSLog(token);
    GROUP_CHAT_ID = -39641397;
    BOT_USERNAME = "WoodyardSandboxBot";
}
var PHOTO_DIR = "images/";
var bot = new TelegramBot(token, {
    polling: true
});

var bannedUsers = {

};
var turnOnDate;
var lookingUp;
var COMMAND_LIST_STATIC = undefined;
var COMMAND_LIST_DETAILS = undefined;
var globally_banned = false;
var admins = [];
var bannedPeople = [];

var http = require("http");
var https = require("https");

var getJSON = function(options, onResult) {
    //NSLog("getJSON");

    var prot = options.port == 443 ? https : http;
    var req = prot.request(options, function(res) {
        var output = '';
        res.setEncoding('utf8');

        res.on('data', function(chunk) {
            output += chunk;
        });

        res.on('end', function() {
            //NSLog("ended");
            var obj = JSON.parse(output);
            onResult(res.statusCode, obj);
        });
    });

    req.on('error', function(err) {
        //NSLog(err);
        //res.send('error: ' + err.message);
    });

    req.end();
};

var download = function(uri, filename, callback) {
    request.head(uri, function(err, res, body) {
        //console.log('content-type:', res.headers['content-type']);
        //console.log('content-length:', res.headers['content-length']);
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};

function findCommandObjectIndex(command) {
    for (index in COMMAND_LIST_STATIC.commands) {
        var commandObject = COMMAND_LIST_DETAILS.commands[index];
        if (commandObject.command.name === command) {
            return index;
        }
    }
    return -1;
}

function findCommandObject(command) {
    var index = findCommandObjectIndex(command);
    if (index != -1) {
        return COMMAND_LIST_DETAILS.commands[index];
    }
    return undefined;
}

function occurrences(string, subString, allowOverlapping) {
    string += "";
    subString += "";
    if (subString.length <= 0) return (string.length + 1);

    var n = 0,
        pos = 0,
        step = allowOverlapping ? 1 : subString.length;

    while (true) {
        pos = string.indexOf(subString, pos);
        if (pos >= 0) {
            ++n;
            pos += step;
        } else break;
    }
    return n;
}

function refuse(command, msg) {
    if (DISABLE_REFUSE) {
        return false;
    }
    if (msg.forward_from) {
        if (msg.forward_from.username === BOT_USERNAME) {
            return true;
        }
    }
    NSLog("Searching for " + command);
    var actualCommandObject = findCommandObject(command);

    NSLog(JSON.stringify(actualCommandObject));

    if (msg.text.toLowerCase().contains("here are all commands") || occurrences(msg.text, "/") > 1) {
        console.log("Asshole trying to send a bunch of commands");
        return true;
    }

    if (actualCommandObject.banned) {
        NSLog(command + " is banned.");
        return true;
    }
    if (!actualCommandObject) {
        NSLog("Didn't find " + command);
        return true;
    }
    if ((msg.chat.type === "group" || msg.chat.type === "supergroup") && (!actualCommandObject.group_enabled)) {
        //bot.sendMessage(msg.chat.id, "This command is not supported in group chats.", { reply_to_message_id:msg.message_id });
        return true;
    }

    return false;
}

function currentlyTyping(msg) {
    bot.sendChatAction(msg.chat.id, "typing");
}

function reloadData(msg) {
    fs.readFile("resources/commands.json", 'utf8', function(err, data) {
        if (err) {
            console.error("ERROR OPENING COMMANDS " + err);
            return;
        }
        COMMAND_LIST_STATIC = JSON.parse(data);
        COMMAND_LIST_DETAILS = COMMAND_LIST_STATIC;
        NSLog("Commands are now enabled.");
        if (msg) {
            bot.sendMessage(msg.chat.id, "Reloaded.", {
                reply_to_message_id: msg.message_id
            });
        }
    });
}

function hourChanged(callback) {
    if (PRODUCTION || FORCE_GRAPH_RELOAD) {
        bot.sendChatAction(GROUP_CHAT_ID, "upload_photo");

        charts.renderDaysChart(function(result) {
            if (!result) {
                NSLog("Success rendering days chart.");
                //bot.sendMessage(EDWIN_ID, "Success rendering days chart!");
            } else {
                bot.sendMessage(EDWIN_ID, "Error rendering days chart: " + JSON.stringify(result));
            }
            charts.renderHoursChart(function(result1) {
                if (!result1) {
                    NSLog("Success rendering hours chart.");
                    //bot.sendMessage(EDWIN_ID, "Success rendering hours chart!");
                } else {
                    bot.sendMessage(EDWIN_ID, "Error rendering hours chart: " + JSON.stringify(result1));
                }
                if (callback) {
                    callback();
                }
                if ((new Date()).getHours() === 23) {
                    setTimeout(function() {
                        bot.sendPhoto(GROUP_CHAT_ID, 'graphs/days.png');
                        setTimeout(function() {
                            bot.sendPhoto(GROUP_CHAT_ID, 'graphs/hours.png');
                            setTimeout(function() {
                                bot.sendMessage(GROUP_CHAT_ID, "Daily stats updated and delivered to your doorstep automagically");
                            }, 1000);
                        }, 500);
                    }, 250);
                }
            });
        });
    } else {
        if (callback) {
            setTimeout(function() {
                callback();
            }, 1000);
        }
        NSLog("Hour changed");
    }
}

function hasAdminStatus(chatID, userID, callback){
    bot.getChatAdministrators(chatID).then(function(adminList){
        var hasStatus = false;
        for(var i = 0; i < admins.length; i++){
            var admin = admins[i];
            if(admin.user.id === userID){
                hasStatus = true;
                break;
            }
        }
        callback(hasStatus);
    });
}

bot.getMe().then(function(me) {
    NSLog('Hello, my name is ' + me.username + ' and I am alive! Commands are turned off for while I load...');

    reloadData(undefined);

    setInterval(function timeChecker() {
        hourChanged();
    }, PRODUCTION ? ((60 * 60 * 1000) * 1.5) : (60000 * 2));

    bot.getChatAdministrators(GROUP_CHAT_ID).then(function(adminList){
        admins = adminList;
        console.log("Admins " + JSON.stringify(admins));
    });

    setInterval(function() {
        bot.getChatAdministrators(GROUP_CHAT_ID).then(function(adminList){
            admins = adminList;
            //console.log("Admin list refreshed.");
        });
    }, 30 * 1000);
});

function getStockChangeString(stockObject, afterhours) {
    var changePercent = parseFloat(!afterhours ? stockObject.cp_fix : stockObject.ecp_fix);
    var changeInValue = parseFloat(!afterhours ? stockObject.c_fix : stockObject.ec_fix);
    changeInValue = Math.abs(changeInValue);
    var status = "*";
    if (changePercent === 0) {
        status += "unchanging";
    } else {
        status += (changePercent < 0) ? "slippin'" : "rising";
    }
    status += "*";

    var string = "";
    if (status === "*unchanging*") {
        string += (afterhours ? "In after hours they" : "They") + " are " + status + ".";
    } else {
        string += (afterhours ? "In after hours they" : "They") + " are " + status + " with a change of " + (!afterhours ? stockObject.cp : stockObject.ecp) + "% ($" + changeInValue + ").";
    }

    return string;
}

function googleFinanceRequest(args) {
    if (args.length === 0) {
        console.error("No arguments!");
        //Todo: make callback?
        return;
    }

    console.log("Got arguments " + JSON.stringify(args));

    (function(arguments) {
        var GOOGLE_URL = "http://finance.google.com/finance/info?client=ig&q=";
        var constructedURL = GOOGLE_URL + arguments.stocks.join();

        console.log("chat id " + arguments.chatID);
        //bot.sendMessage(EDWIN_ID, constructedURL);
        (function(newArgs) {
            request.get({
                url: constructedURL
            }, function(err, httpResponse, body) {
                body = body.substring(3);
                if (body.contains("Response Code")) {
                    //bot.sendMessage(newArgs.chatID, "Invalid symbol or something else went wrong in your request, sorry. Please double check your command.", newArgs.replyTo);
                    return;
                }
                var results = JSON.parse(body);
                for (stockObjectIndex in results) {
                    var stockObject = results[stockObjectIndex];
                    var stockString = "_" + stockObject.t + " on the " + stockObject.e + "_\n\n";
                    stockString += "Last traded for *$" + stockObject.l + "* at " + stockObject.lt + ".";
                    stockString += "\n";
                    stockString += getStockChangeString(stockObject, false);


                    if (stockObject.el !== undefined) {
                        stockString += "\n\n";
                        stockString += "After hours traded for *$" + stockObject.el + "* at " + stockObject.elt + ".\n";
                        stockString += getStockChangeString(stockObject, true);
                    }
                    console.log("Got a reply to " + JSON.stringify(newArgs.replyTo));
                    bot.sendMessage(newArgs.chatID, stockString, newArgs.replyTo);
                }
            });
        })(arguments);
    })(args);

}

bot.onText(/\/quote (.+)/, function(msg, match) {
    if (refuse("quote", msg)) {
        return;
    }
    console.log("got a match");

    match[1] = replaceAll(match[1], " ", "");

    var stockList = match[1].split(",");

    console.log("Got a list of " + JSON.stringify(stockList));

    var requestObject = {
        stocks: stockList,
        chatID: msg.chat.id,
        replyTo: {
            reply_to_message_id: msg.message_id,
            parse_mode: "Markdown"
        }
    };

    if (stockList.length === 0) {
        bot.sendMessage(msg.chat.id, "You've somehow entered in no stocks. Please enter in at least one.", requestObject.replyTo);
        return;
    } else if (stockList.length > 2 && msg.chat.type !== "private") {
        bot.sendMessage(msg.chat.id, "Please enter no more than 2 stocks in group chats, sorry.", requestObject.replyTo);
        return;
    }

    console.log("before");

    console.log("after " + JSON.stringify(requestObject));

    googleFinanceRequest(requestObject);
});

bot.onText(/\/chart (.+)/, function(msg, match) {
    if (refuse("chart", msg)) {
        return;
    }

    var args = match[1].split(" ");
    if (args.length !== 2) {
        bot.sendMessage(msg.chat.id, "Invalid format." +
            " Please enter in the format of [stock symbol] [length, ie. 2d (2 days)].\n\nFor example, '/chart fb 7d'" +
            " would return a chart for symbol FB worth 7 days of data.\n\nYou can use d for days, m for months, and y for years.");
        return;
    }

    var reply = {
        reply_to_message_id: msg.message_id
    }
    console.log("Got a reply of " + JSON.stringify(reply));
    (function(arguments, chatID) {
        var symbol = arguments[0];
        var length = arguments[1];

        var downloadURL = "http://chart.finance.yahoo.com/z?s=" + symbol + "&t=" + length + "&q=c&l=on&z=l";
        var graphFile = "graphs/stock-symbol:" + symbol + "-length:" + length + ".png";
        download(downloadURL, graphFile, function() {
            bot.sendPhoto(chatID, graphFile, reply);
        });
    })(args, msg.chat.id);
});

bot.onText(/\/reload/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    reloadData(msg);
});

bot.onText(/\/kick/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    bot.kickChatMember(msg.chat.id, msg.reply_to_message.from.id);
    setTimeout(function() {
        bot.sendMessage(msg.chat.id, msg.reply_to_message.from.first_name + " has felt the power of 86.");
    }, 750);
});

bot.onText(/\/adminstatus/, function(msg, match) {
    if (refuse("adminstatus", msg)) {
        return;
    }
    if (msg.chat.type !== "group" && msg.chat.type !== "supergroup") {
        bot.sendMessage(msg.chat.id, "This command only works in group chats, sorry");
        return;
    }
    if(msg.reply_to_message){
        var idToLookup = msg.reply_to_message.from.id;
        var nameToSay = msg.reply_to_message.from.first_name + " is";
        hasAdminStatus(msg.chat.id, idToLookup, function(hasStatus){
            var sentence = nameToSay + (hasStatus ? " *blessed with admin status*" : " *simply not worthy of admin status*");
            bot.sendMessage(msg.chat.id, sentence, {reply_to_message_id:msg.message_id, parse_mode:"Markdown"});
        });
    }
    else{
        bot.getChatAdministrators(msg.chat.id).then(function(adminList){
            var adminString = "The following people have admin status in this group:\n*";
            for(var i = 0; i < admins.length; i++){
                var admin = admins[i];
                adminString += admin.user.first_name + ", ";
            }
            adminString = adminString.substring(0, adminString.length-2);
            adminString += "*";
            bot.sendMessage(msg.chat.id, adminString, {parse_mode:"Markdown"});
        });
    }
});

bot.onText(/\/ban/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    bot.kickChatMember(msg.chat.id, msg.reply_to_message.from.id);
    bannedUsers[msg.reply_to_message.from.id + ""] = true;
    setTimeout(function() {
        bot.sendMessage(msg.chat.id, msg.reply_to_message.from.first_name + " has felt ULTIMATE the power of 86.");
    }, 750);
});

bot.onText(/\/unban/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    bannedUsers[msg.reply_to_message.from.id + ""] = false;
    bot.sendMessage(msg.chat.id, msg.reply_to_message.from.first_name + " has been unbanned.");
});

bot.onText(/\/beehive/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }

    BEEHIVE_MODE = !BEEHIVE_MODE;
    bot.sendMessage(msg.chat.id, BEEHIVE_MODE ? "Beehive mode has been enabled. Anyone that talks from this point forward will be automatically kicked." : "Beehive mode has been disabled, enjoy");
});

/*
bot.onText(/\/unban/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    bot.unbanChatMember(msg.chat.id, msg.reply_to_message.from.id);
    setTimeout(function() {
        bot.sendMessage(msg.chat.id, msg.reply_to_message.from.first_name + " has been unbanned.");
    }, 750);
});
*/

bot.onText(/\/abandon/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    bot.leaveChat(msg.chat.id);
});

bot.onText(/\/abandon (.+)/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    bot.leaveChat(parseInt(match[1]));
    bot.sendMessage(msg.chat.id, "Abandoned chat " + match[1]);
});

bot.onText(/\/setchatlink (.+)/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }

    INVITE_LINK = match[1];

    bot.sendMessage(msg.chat.id, "Chat link set.");
});

bot.onText(/\/chatlink/, function(msg, match) {
    bot.sendMessage(msg.chat.id, "[Click me to join The Group Chat.\n](" + INVITE_LINK + ")\nPlease do not forward this message.", {parse_mode:"Markdown"});
});

bot.onText(/\/challenge/, function(msg, match) {
    if (refuse("challenge", msg)) {
        return;
    }
});

bot.onText(/\/roulette/, function(msg, match) {
    if (refuse("roulette", msg)) {
        return;
    }

    for(var i = 0; i < admins.length; i++){
        var admin = admins[i];
        if(admin.user.id === msg.from.id){
            bot.sendMessage(msg.chat.id, "You can't pull the trigger when you have admin status, sorry", {reply_to_message_id:msg.message_id});
            return;
        }
    }

    var kick = false;

    if(!msg.from.username){
        msg.from.username = "nousername";
    }

    if(msg.from.username.toLowerCase() === "petercornelisse"){
        var randomNum = Math.random() * 2;
        kick = Math.ceil(randomNum) === 1;
    }
    else{
        var randomNum = Math.random() * 6;
        kick = Math.ceil(randomNum) === 3;
    }
    //bot.sendMessage(msg.chat.id, "" + kick + " (" + randomNum + ")");
    if(kick){
        var secondsLeft = Math.ceil(Math.random() * 10) + 3;
        bot.sendMessage(msg.chat.id, "*BANG*", {parse_mode:"Markdown"});
        setTimeout(function(){
            console.log("Roulette kicking " + msg.chat.id + " " + msg.from.id);
            bot.kickChatMember(msg.chat.id, msg.from.id);
            bot.sendMessage(msg.from.id, "Lol, you have the luck of Peter.\n\n" + INVITE_LINK);
        }, 500);
    }
    else{
        bot.sendMessage(msg.chat.id, "_click_", {parse_mode:"Markdown"});
    }
});

bot.onText(/\/chatinfo/, function(msg, match) {
    if (refuse("chatinfo", msg)) {
        return;
    }

    if (msg.chat.type !== "group" && msg.chat.type !== "supergroup") {
        bot.sendMessage(msg.chat.id, "This command only works in groups or supergroups, sorry.");
        return;
    }

    bot.getChat(msg.chat.id).then(function(chat) {
        bot.getChatAdministrators(msg.chat.id).then(function(admins) {
            bot.getChatMembersCount(msg.chat.id).then(function(count) {
                var chatObject = {
                    "generalInfo": chat,
                    "admins": admins,
                    "memberCount": count
                };
                var creator = {};
                for (var i = 0; i < admins.length; i++) {
                    if (admins[i].status === "creator") {
                        creator = admins[i];
                        break;
                    }
                }
                var chatString = "This chat is called *" + chat.title + "*.";
                chatString += "\nIt has *" + (count - admins.length) + " regular members* and *" + admins.length + " people with admin status*, that's *" + count + " people in total.*";
                if (creator.user.username && creator.user.first_name) {
                    chatString += "\nThe creator of the group is *" + creator.user.first_name + "* (@" + creator.user.username + ").";
                }
                chatString += "\nThis chat's unique identifier is *" + chat.id + "*.";
                bot.sendMessage(msg.chat.id, chatString, {
                    "parse_mode": "Markdown"
                });
            });
        });
        /*
        NSLog('Hello, my name is ' + me.username + ' and I am alive! Commands are turned off for while I load...');

        reloadData(undefined);

        setInterval(function timeChecker() {
            hourChanged();
        }, PRODUCTION ? ((60*60*1000)*1.5) : (60000*2));
        */
    });
});

function combsort(msg, arguments) {
    var child = require('child_process').spawn(
        "java", arguments
    );
    child.stdout.on('data', function(data) {
        console.log(data.toString());
        bot.sendMessage(msg.chat.id, data.toString());
    });
}

/*
if(msg.from.username !== "Lignite" && msg.from.username !== "VoastMaster"){
    return;
}
*/

bot.onText(/\/combsort/, function(msg, match) {
    return;

    if (msg.text != "/combsort" && msg.text != "/combsort@TheGeneBot") {
        return;
    }

    combsort(msg, ["CombSort"]);
});

bot.onText(/\/combsort (.+)/, function(msg, match) {
    return;

    var arguments = [
        "CombSort"
    ];
    var numbers = match[1].split(" ");
    for (var i = 0; i < numbers.length; i++) {
        arguments.push(numbers[i]);
    }
    combsort(msg, arguments);
});

bot.onText(/\/space (.+)/, function(msg, match) {
    if (refuse("space", msg)) {
        return;
    }

    var newString = "";
    for (var i = 0; i < match[1].length; i++) {
        if (match[1][i] !== " ") {
            newString += match[1][i].toUpperCase() + " ";
        } else {
            newString += "  ";
        }
    }

    var replyTo = {
        reply_to_message_id: msg.message_id
    };
    if (msg.reply_to_message) {
        replyTo = {
            reply_to_message_id: msg.reply_to_message.message_id
        };
    }
    bot.sendMessage(msg.chat.id, newString, replyTo);
});

bot.onText(/\/this/, function(msg, match) {
    if (refuse("this", msg)) {
        return;
    }
    if(msg.text !== "/this" && msg.text !== "/this@" + BOT_USERNAME){
        return;
    }

    var replyTo = {
        reply_to_message_id: msg.message_id,
        parse_mode: "Markdown"
    };
    if (msg.reply_to_message) {
        replyTo = {
            reply_to_message_id: msg.reply_to_message.message_id,
            parse_mode: "Markdown"
        };
    }
    bot.sendMessage(msg.chat.id, "*T H I S*", replyTo);
});

var forceSnowday = "";

function getSnowdayObject(callback) {
    request.post({
        url: 'http://api-ws01.snowdaypredictor.com/calc',
        form: {
            q: 'N1E0H6'
        }
    }, function(err, httpResponse, body) {
        var snowdayObject = JSON.parse(body);
        callback(snowdayObject);
    });
}

bot.onText(/\/rawsnowday/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }

    currentlyTyping(msg);

    if (forceSnowday.length > 0) {
        bot.sendMessage(msg.chat.id, forceSnowday, {
            reply_to_message: msg.message_id
        });
        return;
    }

    getSnowdayObject(function(object) {
        bot.sendMessage(msg.chat.id, JSON.stringify(object), {
            reply_to_message: msg.message_id
        });
    });
});

bot.onText(/\/snowday/, function(msg, match) {
    if (refuse("snowday", msg)) {
        return;
    }

    bot.sendMessage(msg.chat.id, "Fuck you");
});

bot.onText(/\/setsnowday (.+)/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    forceSnowday = match[1];
    if (forceSnowday === "false") {
        forceSnowday = "";
    }
    bot.sendMessage(msg.chat.id, "Snow day set.", {
        reply_to_message: msg.message_id
    });
});

bot.onText(/\/cluck/, function(msg, match) {
    bot.sendMessage(msg.chat.id, "Cluck cluck", {
        reply_to_message: msg.message_id
    });
});

bot.onText(/\/traitor/, function(msg, match) {
    if (refuse("traitor", msg)) {
        return;
    }

    bot.sendChatAction(msg.chat.id, "upload_photo");

    bot.sendPhoto(msg.chat.id, 'traitor.jpg');
});

bot.onText(/\/loyal/, function(msg, match) {
    if (refuse("loyal", msg)) {
        return;
    }

    bot.sendChatAction(msg.chat.id, "upload_photo");

    bot.sendPhoto(msg.chat.id, 'loyal.png');
});
/*
bot.onText(/\/roast (.+)/, function(msg, match){
    if(refuse("roast", msg)){ return; }

    var peter = "" + match[1];

    //NSLog(peter);

    var options = {
        host: 'api.yomomma.info',
        port: 80,
        path: '/',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
        match: peter
    };
    getJSON(options,
        function(statusCode, result){
            // I could work with the result html/json here.  I could also just return it

            //NSLog("match new " + peter);

            var string = result.joke;
            if(peter.indexOf(" ")){
                //peter = peter.substring(0, peter.indexOf(" "));
            }
            peter = capitalizeFirstLetter(peter);
            //NSLog("match " + peter);
            //NSLog("string " + string);
            string = replaceAll(string, "her", "his");
            string = replaceAll(string, "she", "he");
            string = replaceAll(string, "Her", "Him");
            string = replaceAll(string, "She", "He");
            string = replaceAll(string, "Yo mama", (peter));
            string = replaceAll(string, "Yo mamas", (peter + "'s"));
            string = replaceAll(string, "Yo' mommas", (peter + "'s"));
            string = replaceAll(string, "Your mama", (peter + "'s"));
            string = replaceAll(string, "Yo momma", (peter));
            string = replaceAll(string, "Yo mamma", (peter));

            bot.sendMessage(msg.chat.id, string, {reply_to_message:msg.message_id});
        });
});
*/

bot.onText(/\/graphreload/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    bot.sendMessage(msg.chat.id, "Reloading graphs...");
    bot.sendChatAction(msg.chat.id, "upload_photo");
    hourChanged(function() {
        bot.sendMessage(msg.chat.id, "Reloaded!");
    });
});

if (FIX) {
    NSLog("FIX is true, refusing to register commands");
    return;
}

bot.onText(/\/daily/, function(msg, match) {
    if (refuse("daily", msg)) {
        return;
    }

    bot.sendChatAction(msg.chat.id, "upload_photo");

    bot.sendPhoto(msg.chat.id, 'graphs/days.png');
});

bot.onText(/\/monthly/, function(msg, match) {
    if (refuse("monthly", msg)) {
        //return;
    }

    bot.sendChatAction(msg.chat.id, "upload_photo");

    bot.sendPhoto(msg.chat.id, 'graphs/months.png');
});

function nameStringFromMessage(message) {
    if (message.from === undefined) {
        return "Error";
    }
    var name = "";
    if (message.from.first_name !== undefined) {
        name = message.from.first_name;
    } else {
        name = "Unknown";
    }
    if (message.from.last_name !== undefined) {
        name += " " + message.from.last_name;
    }
    return name;
}

bot.onText(/\/refreshchatmembers/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    console.log("Reload");
    analytics.readFromDatabase(false, {}, function(error, results) {
        if (error) {
            return console.error(error);
        }
        //bot.sendMessage(msg.chat.id, "" + results.length);
        var data = {};
        var names = {};
        for (var i = 0; i < results.length; i++) {
            var message = results[i].message;
            var name = message.from.id + "";
            if (data[name] === undefined) {
                data[name] = 1;
            } else {
                data[name] += 1;
            }
            names[name] = nameStringFromMessage(message);
        }

        if(data[LUKA_ID + ""] > data[EDWIN_ID + ""]){
            data[LUKA_ID + ""] = (data[EDWIN_ID + ""]-1);
        }

        var namesArray = [];
        var valuesArray = [];
        var i = 0;
        for (var key in data) {
            if (data[key] >= 100) {
                namesArray[i] = names[key];
                valuesArray[i] = data[key];
                i++;
            }
        }

        var swapped;
        do {
            swapped = false;
            for (var i = 0; i < valuesArray.length-1; i++) {
                if(valuesArray[i] > valuesArray[i+1]){
                    var temp = valuesArray[i];
                    valuesArray[i] = valuesArray[i+1];
                    valuesArray[i+1] = temp;

                    var temp1 = namesArray[i];
                    namesArray[i] = namesArray[i+1];
                    namesArray[i+1] = temp1;

                    swapped = true;
                }
            }
        } while (swapped);

        valuesArray.reverse();
        namesArray.reverse();

        charts.renderGroupChatMembersChart(namesArray, valuesArray, function(error) {
            if (error) {
                console.log("\nERRORROROROROR\n");
                console.error(error);
                return;
            }
            console.log("Success");
        });
        //bot.sendMessage(msg.chat.id, JSON.stringify(data));
    });
});

bot.onText(/\/chatmembers/, function(msg, match) {
    if (refuse("chatmembers", msg)) {
        return;
    }

    bot.sendChatAction(msg.chat.id, "upload_photo");

    bot.sendPhoto(msg.chat.id, 'graphs/group_chat_members.png');
});

bot.onText(/\/fuck/, function(msg, match) {
    bot.sendChatAction(msg.chat.id, "upload_photo");

    bot.sendPhoto(msg.chat.id, 'graphs/group_chat_members.png');
});

bot.onText(/\/chatnames/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    //if(refuse("daily", msg)){ return; }

    bot.sendChatAction(msg.chat.id, "upload_photo");

    bot.sendPhoto(msg.chat.id, 'graphs/group_chat_names.png');
});

bot.onText(/\/refreshchatnames/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    console.log("testing");
    analytics.getTotalCount(function(count) {
        analytics.getTLDRData(count, count, function(error, docs) {
            console.log("totalsent " + docs.length + " and first message " + docs[0]);
            var data = {};
            for (var i = 0; i < docs.length; i++) {
                if (data[docs[i].message.chat.title]) {
                    data[docs[i].message.chat.title] += 1;
                } else {
                    data[docs[i].message.chat.title] = 1;
                }
            }
            var namesArray = [];
            var valuesArray = [];
            var i = 0;
            for (var key in data) {
                if (key.length < 25 && data[key] >= 1250) {
                    namesArray[i] = key;
                    valuesArray[i] = data[key];
                    i++;
                }
            }

            var swapped;
            do {
                swapped = false;
                for (var i = 0; i < valuesArray.length-1; i++) {
                    if(valuesArray[i] > valuesArray[i+1]){
                        var temp = valuesArray[i];
                        valuesArray[i] = valuesArray[i+1];
                        valuesArray[i+1] = temp;

                        var temp1 = namesArray[i];
                        namesArray[i] = namesArray[i+1];
                        namesArray[i+1] = temp1;

                        swapped = true;
                    }
                }
            } while (swapped);

            valuesArray.reverse();
            namesArray.reverse();

            var finalData = {
                names: namesArray,
                values: valuesArray
            }
            console.log("\n" + namesArray.length + "\n");

            charts.renderGroupChatNamesChart(namesArray, valuesArray, function(error) {
                if (error) {
                    console.log("\nERRORROROROROR\n");
                    console.error(error);
                    return;
                }
                console.log("Success");
            });

            //console.log(JSON.stringify(finalData));
        });
    });
});

bot.onText(/\/hourly/, function(msg, match) {
    if (refuse("hourly", msg)) {
        return;
    }

    bot.sendChatAction(msg.chat.id, "upload_photo");

    bot.sendPhoto(msg.chat.id, 'graphs/hours.png');
});

bot.onText(/\/lit/, function(msg, match) {
    if (refuse("lit", msg)) {
        return;
    }

    var currentTime = Math.floor(Date.now() / 1000);

    var howLitData = {
        secondly: 0,
        minutely: 0,
        hourly: 0,
        daily: 0
    };

    analytics.getCountForSearch({
        "message.date": {
            "$gt": currentTime - (86400 * 3),
            "$lt": currentTime
        }
    }, 0, function(data) {
        howLitData.daily = (data.count / 3).toFixed(1);
        analytics.getCountForSearch({
            "message.date": {
                "$gt": currentTime - (3600 * 3),
                "$lt": currentTime
            }
        }, 0, function(data) {
            howLitData.hourly = (data.count / 3).toFixed(1);
            analytics.getCountForSearch({
                "message.date": {
                    "$gt": currentTime - (60 * 3),
                    "$lt": currentTime
                }
            }, 0, function(data) {
                howLitData.minutely = (data.count / 3).toFixed(1);
                howLitData.secondly = (data.count / 60).toFixed(1);

                var litString = "";
                var totallyLit = false;
                if (howLitData.minutely >= 21) {
                    litString = "FIRE FIRE FIRE FIRE\nCHAT IS TOTALLY FUCKING LIT\n\n";
                    totallyLit = true;
                } else if (howLitData.minutely >= 13) {
                    litString = "Holy shit, group chat is absolutely LIT\n\n";
                } else if (howLitData.minutely > 5) {
                    litString = "Chat's pretty lit I have to say\n\n";
                } else {
                    litString = "Chat's not lit, flame has been stomped on by " + msg.from.first_name + " \n\n";
                }
                litString += howLitData.secondly + " per second\n";
                litString += howLitData.minutely + " per minute\n";
                litString += howLitData.hourly + " per hour\n";
                litString += howLitData.daily + " per day\n\n";
                if (totallyLit) {
                    litString = litString.toUpperCase();
                }
                bot.sendMessage(msg.chat.id, litString);
            });
        });
    });
});

bot.onText(/\/testread/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    analytics.readFromDatabase({
        "test": true
    }, function(error, data) {
        if (!error) {
            bot.sendMessage(msg.chat.id, "Success reading.", {
                reply_to_message_id: msg.message_id
            });
        } else {
            bot.sendMessage(msg.chat.id, "Failed reading. Error: " + error, {
                reply_to_message_id: msg.message_id
            });
        }
    });
});

bot.onText(/\/testwrite/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    analytics.writeToDatabase([{
        "time": ((new Date()).getTime()),
        "test": true
    }, {
        "hello": ((new Date()).getTime())
    }], function(error, data) {
        if (!error) {
            bot.sendMessage(msg.chat.id, "Success writing.", {
                reply_to_message_id: msg.message_id
            });
        } else {
            bot.sendMessage(msg.chat.id, "Failed writing. Error: " + error, {
                reply_to_message_id: msg.message_id
            });
        }
    });
});

bot.onText(/\/tldr (.+)/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        bot.sendMessage(msg.chat.id, "You fuckers didn't appreciate the /tldr command enough so I removed it", {
            reply_to_message_id: msg.message_id
        });
        return;
    }

    if (msg.chat.type === "group") {
        bot.sendMessage(msg.chat.id, "To prevent spamming, please message me for a TL;DR privately", {
            reply_to_message_id: msg.message_id
        });
        return;
    }

    var spaceLocation = match[1].indexOf(" ");
    var number = parseInt(match[1].substring(spaceLocation + 1));
    if (!number) {
        bot.sendMessage(msg.chat.id, "Invalid format, please enter in format:\n/tldr last 60", {
            reply_to_message_id: msg.message_id
        });
        return;
    }

    var totalCount;
    analytics.getTotalCount(function(count) {
        totalCount = count;

        if (number > (totalCount - 1)) {
            bot.sendMessage(msg.chat.id, "Sorry, you can't request more than " + (totalCount - 1) + " messages.", {
                reply_to_message_id: msg.message_id
            });
            return;
        }

        analytics.getTLDRData(totalCount, number, function(error, docs) {
            if (error) {
                bot.sendMessage(msg.chat.id, "Windows error: " + error + ", sorry.", {
                    reply_to_message_id: msg.message_id
                });
                return;
            }
            var tldr_string = "Begin TL;DR last " + number + ": \n\n";
            var tldr_docs = [];
            var amountToGrab = Math.ceil(number * 0.1);
            var sectionGrabAmount = Math.ceil(amountToGrab * 0.1);
            NSLog("Amount to grab " + amountToGrab + " and sectionGrabAmount " + sectionGrabAmount + " and docs " + docs.length);
            if (sectionGrabAmount < 1) {
                sectionGrabAmount = 1;
            }
            for (var i = 0; i < 10; i++) {
                for (var i1 = 0; i1 < sectionGrabAmount; i1++) {
                    var arrayLocation = (i * amountToGrab) + (i1 * Math.floor(Math.random() * sectionGrabAmount + 1));
                    if (arrayLocation < docs.length) {
                        tldr_docs.push(docs[arrayLocation]);
                    }
                }
            }
            for (var i = 0; i < tldr_docs.length; i++) {
                var textToSend = tldr_docs[i].message.text;
                if (textToSend) {
                    if (tldr_docs[i].message.forward_from) {
                        textToSend = "[forwarded message from " + tldr_docs[i].message.forward_from.first_name + "]";
                    }
                    var date = new Date(parseInt(tldr_docs[i].message.date) * 1000);
                    var dateString = date.toTimeString().substring(0, 5);
                    tldr_string += tldr_docs[i].message.from.first_name + " @ " + dateString + ": '" + textToSend + "'\n";
                }
            }
            if (tldr_string.length > 3900) {
                var amountOfMessages = Math.ceil(tldr_string.length / 3900);
                for (var i = 0; i < amountOfMessages; i++) {
                    var message = tldr_string.substring(3900 * i, 3900 * (i + 1));
                    bot.sendMessage(msg.chat.id, "Part " + (i + 1) + ":\n\n" + message, {
                        reply_to_message_id: msg.message_id
                    });
                }
            } else {
                tldr_string += "---\nDisclaimer: TL;DRs are made up of 10% of the last N messages you chose. Please keep in mind that as you expand N, the gap between messages" +
                    " becomes signifigantly larger and they make less and less sense.";
                bot.sendMessage(msg.chat.id, tldr_string, {
                    reply_to_message_id: msg.message_id
                });
            }
        });
    });
});

bot.onText(/\/totalsent/, function(msg, match) {
    if (refuse("totalsent", msg)) {
        return;
    }

    analytics.getTotalCount(function(count) {
        if (count) {
            analytics.getTotalCountSinceNewYear(function(newYearCount) {
                var totalSentString = "";
                totalSentString += newYearCount + " messages have been sent since January 1st, 2016.\n\n";
                totalSentString += count + " messages have been sent since December 13th, 2015.";

                bot.sendMessage(msg.chat.id, totalSentString, {
                    reply_to_message_id: msg.message_id
                });
            });
        }
    });
});

function getLurkerString(percentage, first_name) {
    var lurkerString = "";
    if (percentage > 10) {
        lurkerString += "❎ " + first_name + " is an anti-lurker ❎\n\nYou are an anti-lurker. You're literally in the process of ordering group chat t-shirts with your favourite stickers on them. You make cakes with references to groupchat stickers and always talk down to those who aren't in the group chat.";
    } else if (percentage > 5 && percentage <= 10) {
        lurkerString += "❎ " + first_name + " is a good chat member ❎\n\nYou are a good group chat member. You contribute daily to the group chat, from constantly checking the snowday Hassan to roasting Alex.";
    } else if (percentage >= 3 && percentage <= 5) {
        lurkerString += "😶 " + first_name + " is close to a lurker 😶\n\nYou are on the verge of being a lurker 😥. Make sure you post fresh memes and roasts to the chat daily and you will make it. Otherwise you'll be in the outlands like Alex.";
    } else if (percentage < 3 && percentage >= 2) {
        lurkerString += "✅ " + first_name + " is a lurker ✅\n\nYou are a lurker 😶. Although you do not contribute much to the group, there is still hope for you down the road. Make sure to contribute outside of your comfort zone and you won't end up as an ultimate lurker.";
    } else if (percentage < 2 && percentage > 0) {
        lurkerString += "✅ " + first_name + " is a HARDCORE lurker ✅\n\nYou are a hardcore lurker 😢. You have barely contribute to the group, and when you do, it's shitty reposts of stuff that's already been forwarded to the classics archive long before your brain even developed the slightest concept of what a meme was.";
    } else {
        lurkerString += "✅ " + first_name + " is an ULTIMATE lurker ✅\n\nYou are one of the top lurkers. You have not contributed anything to the group and should probably just pull the trigger. There's barely any hope for you to get your lurker status to the famous 'anti-lurker'. It's literally in your fucking Genes.";
    }
    return lurkerString;
}

bot.onText(/\/lurkerstatus/, function(msg, match) {
    if (refuse("lurkerstatus", msg)) {
        return;
    }

    analytics.getTotalCountSinceNewYear(function(totalCount) {
        analytics.getUserStats(msg.from.id, function(stats) {
            if (stats) {
                var percentage = Math.floor((stats.sent / totalCount) * 100);
                var lurkerString = getLurkerString(percentage, msg.from.first_name);

                bot.sendMessage(msg.chat.id, lurkerString, {
                    reply_to_message_id: msg.message_id
                });
            } else {
                bot.sendMessage(msg.chat.id, "I couldn't fetch that user's stats. Are you sure they exist?");
            }
        });
    });
});

bot.onText(/\/mystats/, function(msg, match) {
    if (refuse("mystats", msg)) {
        return;
    }

    analytics.getTotalCountSinceNewYear(function(totalCount) {
        analytics.getUserStats(msg.from.id, function(stats) {
            if (stats) {
                var statString = "Stats for " + msg.from.first_name + " from Jan. 1st:\n\n";
                statString += "Sent " + stats.sent + " messages to the group chat.\n";
                statString += msg.from.first_name + " accounts for about " + ((stats.sent / totalCount) * 100).toFixed(2) + "% of the group chat messages.\n";
                var percentage = Math.floor((stats.sent / totalCount) * 100);
                var lurkerString = getLurkerString(percentage, msg.from.first_name);
                statString += "Lurker status: " + lurkerString.substring(0, lurkerString.indexOf("\n\n")) + "\n";
                statString += "Telegram account #" + msg.from.id + ".\n";
                statString += "Gene loves you <3";

                bot.sendMessage(msg.chat.id, statString, {
                    reply_to_message_id: msg.message_id
                });
            } else {
                bot.sendMessage(msg.chat.id, "I couldn't fetch that user's stats. Are you sure they exist?");
            }
        });
    });
});

bot.onText(/\/306/, function(msg, match) {
    if (refuse("306", msg)) {
        return;
    }

    bot.forwardMessage(msg.chat.id, EDWIN_ID, 94);
});

bot.onText(/\gover2/, function(msg, match) {
    if (refuse("gover2", msg)) {
        return;
    }

    bot.forwardMessage(msg.chat.id, EDWIN_ID, 95);
});

bot.onText(/\/witchhunt/, function(msg, match) {
    if (refuse("witchhunt", msg)) {
        return;
    }

    bot.forwardMessage(msg.chat.id, EDWIN_ID, 96);
});

bot.onText(/\/off (.+)/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        bot.sendMessage(msg.chat.id, "Sorry, you don't have permission to press the big red button.", {
            reply_to_message_id: msg.message_id
        });
        return;
    }

    var colonIndex = match[1].indexOf(":");
    var minutes = parseInt(match[1].substring(colonIndex + 1));
    var commandString = match[1].substring(0, colonIndex);
    var commandObjectIndex = findCommandObjectIndex(commandString);
    var commandObject = findCommandObject(commandString);
    commandObject.banned = true;
    COMMAND_LIST_DETAILS.commands[commandObjectIndex] = commandObject;

    bot.sendMessage(msg.chat.id, "Disabled. Command '/" + commandObject.command.name + "' is now shutdown for " + minutes + " minutes.");

    setTimeout(function() {
        commandObject.banned = false;
        COMMAND_LIST_DETAILS.commands[commandObjectIndex] = commandObject;
        NSLog(commandObject.command.name + " command is now re-enabled.");
    }, (minutes * 60 * 1000));
});

bot.onText(/\/on (.+)/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        bot.sendMessage(msg.chat.id, "Sorry, you don't have permission to press the big green button.", {
            reply_to_message_id: msg.message_id
        });
        return;
    }

    var commandString = match[1];
    var commandObjectIndex = findCommandObjectIndex(commandString);
    var commandObject = findCommandObject(commandString);
    commandObject.banned = false;
    COMMAND_LIST_DETAILS.commands[commandObjectIndex] = commandObject;

    bot.sendMessage(msg.chat.id, "Command '/" + commandString + "' is now completely enabled.");
});

function handleReply(msg) {
    switch (msg.reply_to_message.text) {
        case "What period? (1-4)":
            provideExamDate(msg);
            break;
    }
}

var lastID = 0;

function download(uri, filename, callback) {
    request.head(uri, function(err, res, body) {
        //console.log('content-type:', res.headers['content-type']);
        //console.log('content-length:', res.headers['content-length']);
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};

// Any kind of message
bot.on('message', function(msg) {
    if (msg.date === lastID) {
        NSLog("Windows error");
        return;
    }
    lastID = msg.date;

    NSLog(JSON.stringify(msg));
    ////NSLog("Message ID: " + msg.message_id + " and chat ID: " + msg.chat.id);

    if (PRODUCTION) {
        if (msg.chat.id === GROUP_CHAT_ID && msg.chat.type === "group") { //Make sure it's from "The Almighty Group Chat"
            if (msg.from.id !== WILL_DENNIS) { //Don't include Will in the analytics
                analytics.writeToDatabase(false, {
                    "message": msg
                }); //Store message in database
            }
        }
    }

    if(BEEHIVE_MODE && msg.chat.id === GROUP_CHAT_ID){
        var isAdmin = false;
        for(var i = 0; i < admins; i++){
            var admin = admins[i];
            if(admin.user.id === msg.from.id){
                isAdmin = true;
                break;
            }
        }
        if(!isAdmin){
            bot.kickChatMember(GROUP_CHAT_ID, msg.from.id);
            return;
        }
    }

    /*
        analytics.getTotalCount(function(count){
            if(count === 100000 && (msg.chat.id === GROUP_CHAT_ID)){
                bot.sendMessage(msg.chat.id, "HOLY FUCK MONUMENTAL MOMENT WITH THAT MEME-MESSAGE WE JUST BROKE 100,000 MESSAGES SENT SINCE DECEMBER 13TH 2015 I HOPE THAT WAS WORTH A FORWARD TO THE CLASSICS ARCHIVE BECAUSE YOU'RE THE ONE THAT CAUSED THE GROUP TO FORMALLY MARK THE WASTE OF THOUSANDS OF HOURS COLLABERATIVELY WITH OVER 100,000 MESSAGES IN UNDER THREE MONTHS GOOD JOB BUDDY WOOHOOOOOOOOOO", { reply_to_message_id:msg.message_id });
                bot.sendMessage(msg.chat.id, "(would be a good idea to click /totalsent rn)");
            }
        });
        */
    /*
    //Submit photos for the woman folder
        if(msg.photo && msg.chat.type === "private"){
            console.log("Is a photo " + msg.photo.length-1);
            var id = msg.photo[msg.photo.length-1].file_id;
            console.log("ID ID ID " + id);
            bot.getFileLink(id).then(function (response) {
                //DOWNLOAD FILE
                var location = "photos/" + Math.floor(Math.random()*100000) + "--" + msg.from.id + ".jpg";
                download(response, location, function(){
                    bot.sendMessage(msg.chat.id, "Woman saved to " + location + ", thanks");
                });
            });
        }
        */

    if (msg.photo && msg.chat.type === "private") {
        bot.sendMessage(msg.chat.id, "I'm no longer accepting submissions for the woman folder, sorry");
    }

    //{"message_id":68894,"from":{"id":EDWIN_ID,"first_name":"Edwin","username":"Lignite"},"chat":{"id":EDWIN_ID,"first_name":"Edwin","username":"Lignite","type":"private"},"date":1453159246,"photo":[{"file_id":"AgADAQADNcQxG2nOFgAB1rA2ReALl-zYBdspAATMDBjCQbVLbRRxAQABAg","file_size":804,"width":90,"height":56},{"file_id":"AgADAQADNcQxG2nOFgAB1rA2ReALl-zYBdspAASQol2RmuJu6BNxAQABAg","file_size":16141,"width":320,"height":200},{"file_id":"AgADAQADNcQxG2nOFgAB1rA2ReALl-zYBdspAARWz-iAA6ex-xJxAQABAg","file_size":91596,"width":800,"height":500},{"file_id":"AgADAQADNcQxG2nOFgAB1rA2ReALl-zYBdspAASQwWWb8soZnxFxAQABAg","file_size":159091,"width":1280,"height":800}]}

    if (msg.reply_to_message) {
        //NSLog("Is a reply...");
        if (msg.reply_to_message.from.username === BOT_USERNAME) {
            //NSLog("Is woodyard bot!");
            handleReply(msg);
            return;
        }
    }

    if (msg.new_chat_participant) {
        //NSLog("New participant");

        if(bannedUsers[msg.new_chat_participant.id + ""] === true){
            bot.sendMessage(msg.chat.id, "You're banned sucker");
            setTimeout(function(){
                bot.kickChatMember(msg.chat.id, msg.new_chat_participant.id);
            }, 500);
            return;
        }
        else{
            currentlyTyping(msg);
            setTimeout(function() {
                if (msg.new_chat_participant.username === BOT_USERNAME) {
                    //NSLog("I joined");
                    bot.sendMessage(msg.chat.id, "Nice nice. Thanks for adding me. Let's repair the wires");
                } else {
                    //NSLog("Someone else joined");
                    bot.sendMessage(msg.chat.id, "Nice, nice. Welcome " + msg.new_chat_participant.first_name + ". What kind of hats do you have?");
                }
            }, 1200);
        }
    } else if (msg.left_chat_participant) {
        //NSLog("participant left");
        if (msg.left_chat_participant.username !== BOT_USERNAME) {
            currentlyTyping(msg);
            setTimeout(function() {
                //NSLog("Someone dropped");
                bot.sendMessage(msg.chat.id, "Suck, " + msg.left_chat_participant.first_name + " was forgotten faster than me when I moved into a shack in Alberta.");
            }, 1400);
        }
    }

    if (msg.text.toLowerCase().contains("nice nice")) {
        var opts = {
            reply_to_message_id: msg.message_id,
        };
        bot.sendMessage(msg.chat.id, 'Nice nice', opts);
    } else if (msg.text.toLowerCase().contains("genes's package")) {
        var opts = {
            reply_to_message_id: msg.message_id,
        };
        bot.sendMessage(msg.chat.id, "I'm gonna cut the wires on that package", opts);
    }
    /*
    else if(msg.text.toLowerCase().contains("wires")){
  		var opts = {
  			reply_to_message_id: msg.message_id,
		};
		bot.sendMessage(msg.chat.id, '😏💥', opts);
	}
    */
    else if (msg.text.toLowerCase() === "hi" && msg.chat.id !== CREIGHTON_CREW) {
        var opts = {
            reply_to_message_id: msg.message_id,
        };
        bot.sendMessage(msg.chat.id, "Don't *FUCKING* say " + msg.text, opts);
    }
});


bot.onText(/\/echo (.+)/, function(msg, match) {
    if (refuse("echo", msg)) {
        return;
    }

    var chatId = msg.chat.id;
    var resp = match[1];
    bot.sendMessage(chatId, resp);
});

bot.onText(/\/now/, function(msg, match) {
    if (refuse("now", msg)) {
        return;
    }

    var now = Date.now();
    bot.sendMessage(msg.chat.id, "Epoch Time\nMillis: " + now + "\nSeconds: " + (now / 1000).toFixed(0));
});

bot.onText(/\/epoch (.+)/, function(msg, match) {
    if (refuse("epoch", msg)) {
        return;
    }

    var epoch, value = parseInt(match[1]),
        assumeMillis = false;
    if (value > 1455500000000) {
        assumeMillis = true;
    } else {
        value = value * 1000;
    }
    var epoch = new Date(value);
    bot.sendMessage(msg.chat.id, (assumeMillis ? "(Assuming milliseconds)\n" : "") + match[1] + " to human readable date:\n" + epoch);
});

bot.onText(/\/speciallesson/, function(msg, match) {
    if (refuse("speciallesson", msg)) {
        return;
    }

    bot.forwardMessage(msg.chat.id, EDWIN_ID, 97);
});

bot.onText(/\/christmas/, function(msg, match) {
    if (refuse("christmas", msg)) {
        return;
    }

    var christmas = new Date("Dec 25, 2016");
    var now = new Date();
    bot.sendMessage(msg.chat.id, "There are about " + (Math.floor((christmas.getTime() - now.getTime()) / 1000 / 60 / 60)) + " hours until Christmas! That's " + (christmas.getTime() - now.getTime()) + " milliseconds.", {
        reply_to_message_id: msg.message_id
    });
    setTimeout(function() {
        bot.forwardMessage(msg.chat.id, EDWIN_ID, 98); //with every millisecond I become exponentially more excited
    }, 1000);
});

bot.onText(/\/countdownto (.+)/, function(msg, match) {
    if (refuse("countdownto", msg)) {
        return;
    }

    var dateToCountdownTo = new Date(match[1]);
    if (dateToCountdownTo === undefined) {
        bot.sendMessage(msg.chat.id, "Invalid format. Please enter in the following format: [Mo day, year]. For example:\n\nDec 25, 2015", {
            reply_to_message_id: msg.message_id
        });
        return;
    }
    var now = new Date();
    if (isNaN(Math.floor((dateToCountdownTo.getTime() - now.getTime()) / 1000))) {
        bot.sendMessage(msg.chat.id, "Remove " + msg.from.first_name);
    } else {
        bot.sendMessage(msg.chat.id, "There are about " + (Math.floor((dateToCountdownTo.getTime() - now.getTime()) / 1000 / 60 / 60)) + " hours until then! That's " + Math.floor((dateToCountdownTo.getTime() - now.getTime()) / 1000) + " seconds.", {
            reply_to_message_id: msg.message_id
        });
    }
});

var currentlyAskingForExamDate;

function provideExamDate(msg) {
    var replyText = false;
    switch (msg.text.toLowerCase()) {
        case "one":
        case "1":
            replyText = "June 21, 2016 9:15";
            break;
        case "two":
        case "2":
            replyText = "June 22, 2016 9:15";
            break;
        case "three":
        case "3":
            replyText = "June 23, 2016 9:15";
            break;
        case "four":
        case "4":
            replyText = "June 24, 2016 9:15";
            break;
        default:
            replyText = false;
            break;
    }
    if (!replyText) {
        bot.sendMessage(msg.chat.id, "Invalid period. Please reply to the examdate message with only a period number.", {
            reply_to_message_id: msg.message_id
        });
    } else {
        var d = new Date(replyText);
        var currentDate = new Date();
        var replyString = "Your exam will be around " + replyText + ".\n\nThat means you have aboot " + Math.floor((d.getTime() - currentDate.getTime()) / 1000 / 60 / 60) + " hours to study. (That's " + (d.getTime() - currentDate.getTime()) + " milliseconds.)";
        bot.sendMessage(msg.chat.id, replyString, {
            reply_to_message_id: msg.message_id
        });
    }
}

bot.onText(/\/examdate/, function(msg, match) {
    if (refuse("examdate", msg)) {
        return;
    }

    //currentlyTyping();

    var chatId = msg.chat.id;
    currentlyAskingForExamDate = msg.from.id;
    var opts = {
        reply_to_message_id: msg.message_id
    };
    bot.sendMessage(chatId, 'What period? (1-4)', opts);
});

bot.onText(/\/help/, function(msg, match) {
    if (refuse("help", msg)) {
        if (msg.forward_from) {
            if (msg.forward_from.username === BOT_USERNAME) {
                return;
            }
        }
        return;
    }

    if (!COMMAND_LIST_STATIC) {
        NSLog("Command list is not initialized yet, refusing.");
        bot.sendMessage(msg.chat.id, "Command failed. Please try again.", {
            reply_to_message_id: msg.message_id
        });
        return;
    }

    var commandList = COMMAND_LIST_DETAILS;
    var isGroupChat = (msg.chat.type === "group") || (msg.chat.type === "supergroup");
    var helpString = "";

    if (isGroupChat) {
        bot.sendMessage(msg.chat.id, "Please ask for help in your private chat with me.");
        return;
    }

    helpString += "Here are all commands available. Commands with a *[private]* beside them are private chat only.\n";

    NSLog("Got " + commandList.commands.length);

    var helpString;
    for (index in commandList.commands) {
        var commandObject = commandList.commands[index];
        //NSLog(JSON.stringify(commandObject));
        var objectString = "/" + commandObject.command.name + " ";
        if (commandObject.command.parameters) {
            objectString += commandObject.command.parameters + " ";
        }
        objectString += "- ";
        if (!commandObject.group_enabled) {
            objectString += " *[private]* ";
        }
        objectString += commandObject.description;

        if (!commandObject.isBanned && !commandObject.hide_from_help) {

            helpString += objectString;

            helpString += "\n";
        }
    }

    NSLog(helpString);
    bot.sendMessage(msg.chat.id, helpString, {
        reply_to_message_id: msg.message_id,
        parse_mode: "Markdown"
    });
});

bot.onText(/\/define (.+)/, function(msg, match) {
    if (refuse("define", msg)) {
        return;
    }
    var definition = urban(match[1]);

    definition.first(function(json) {
        if (json) {
            bot.sendMessage(msg.chat.id, json.definition, {
                reply_to_message_id: msg.message_id
            });
        } else {
            bot.sendMessage(msg.chat.id, "Undefined, just like your penis", {
                reply_to_message_id: msg.message_id
            });
        }
    });
});

var AMOUNT_OF_WOMEN_AVAILABLE = 265;

bot.onText(/\/woman/, function(msg, match) {
    if (refuse("woman", msg)) {
        return;
    }

    bot.sendChatAction(msg.chat.id, "upload_photo");

    var photoID = Math.floor(Math.random() * AMOUNT_OF_WOMEN_AVAILABLE);
    var photo = "women/" + photoID + ".jpg";

    bot.sendPhoto(msg.chat.id, photo);
});

bot.onText(/\/source/, function(msg, match) {
    if (refuse("source", msg)) {
        return;
    }

    bot.sendMessage(msg.chat.id, "Here's my source code: https://gitlab.com/edwinfinch/woodyardbot/tree/master");
});

bot.onText(/\/flipacoin/, function(msg, match) {
    if (refuse("flipacoin", msg)) {
        return;
    }

    var result = (Math.floor(Math.random() * 2));
    var rare = (Math.floor(Math.random() * 100));
    if (rare === 69) {
        bot.sendMessage(msg.chat.id, "Sideways, try again", {
            reply_to_message_id: msg.message_id
        });
        return;
    }
    bot.sendMessage(msg.chat.id, (result == 1) ? "Heads" : "Tails", {
        reply_to_message_id: msg.message_id
    });
});

function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

var stats = 0;

bot.onText(/\/userinfo/, function(msg, match) {
    if (refuse("userinfo", msg)) {
        return;
    }

    if (msg.reply_to_message === undefined) {
        bot.sendMessage(msg.chat.id, "Please use this command by replying to a user's message with it.", {
            reply_to_message_id: msg.message_id
        });
        return;
    }
    bot.getChatMember(msg.chat.id, msg.reply_to_message.from.id).then(function(telegramUser) {
        analytics.getTotalCountSinceNewYear(function(totalCount) {
            analytics.getUserStats(msg.reply_to_message.from.id, function(stats) {
                console.log("Got stats " + JSON.stringify(stats));
                if (stats) {
                    var lookupString = "";
                    var percentage = ((stats.sent / totalCount) * 100);
                    var lurkerString = getLurkerString(Math.floor(percentage), msg.reply_to_message.from.first_name);

                    var totalSentStat = stats.sent;
                    if(msg.reply_to_message.from.id === LUKA_ID){
                        totalSentStat = stats.edwinSent-1;
                    }

                    var stats = {
                        rawPercentage: percentage.toFixed(2),
                        totalSentSinceJan: totalSentStat,
                        lurkerStatus: lurkerString.substring(0, lurkerString.indexOf("\n\n")),
                        telegramInfo: telegramUser
                    };
                    lookupString += JSON.stringify(stats, null, 4);

                    bot.sendMessage(msg.chat.id, lookupString, {
                        reply_to_message_id: msg.message_id
                    });
                }
            });
        });
    });
});

bot.onText(/\/whatdoiown/, function(msg, match) {
    if (refuse("whatdoiown", msg)) {
        return;
    }

    if (msg.chat.type !== "private") {
        bot.sendMessage(msg.chat.id, "To prevent spamming like Luka when he first used the bot, please only use this command in private chat: @TheGeneBot", {
            reply_to_message_id: msg.message_id
        });
        return;
    }
    fs.readFile("resources/thoughts.json", 'utf8', function(err, data) {
        if (err) {
            console.error("Could not open file: %s", err);
            bot.sendMessage(msg.chat.id, "I had some trouble gathering my thoughts (" + err + ")", {
                reply_to_message_id: msg.message_id
            });
            return;
        }
        var jsonData = JSON.parse(data);
        var owns = [];
        for (key in jsonData) {
            if (jsonData[key].owner === msg.from.id) {
                owns.push(key);
            }
        }
        bot.sendMessage(msg.chat.id, "Please, DO NOT forward this message! If you do, you're just a dick, sorry. Thoughts are more fun when they are anonymous.\n\nYou own the following thoughts: " + owns, {
            reply_to_message_id: msg.message_id
        });
    });
});

bot.onText(/\/allthoughts/, function(msg, match) {
    if (refuse("allthoughts", msg)) {
        return;
    }

    if (msg.from.username !== "Lignite") {
        bot.sendMessage(msg.chat.id, "You can't do that, sorry", {
            reply_to_message_id: msg.message_id
        });
        return;
    }
    fs.readFile("resources/thoughts.json", 'utf8', function(err, data) {
        if (err) {
            console.error("Could not open file: %s", err);
            bot.sendMessage(msg.chat.id, "I had some trouble gathering my thoughts (" + err + ")", {
                reply_to_message_id: msg.message_id
            });
            return;
        }
        var jsonData = JSON.parse(data);
        var owns = [];
        for (key in jsonData) {
            owns.push(key);
        }
        bot.sendMessage(msg.chat.id, owns.length + " thoughts: " + owns, {
            reply_to_message_id: msg.message_id
        });
    });

});

bot.onText(/\/start/, function(msg, match) {
    if (refuse("start", msg)) {
        return;
    }

    bot.sendMessage(msg.chat.id, "/stop", {
        reply_to_message_id: msg.message_id
    });
});

bot.onText(/\/stop/, function(msg, match) {
    if (refuse("stop", msg)) {
        return;
    }

    bot.forwardMessage(msg.chat.id, EDWIN_ID, 99);
});

bot.onText(/\/squareup/, function(msg, match) {
    if (refuse("squareup", msg)) {
        return;
    }

    bot.forwardMessage(msg.chat.id, EDWIN_ID, 100);
});

bot.onText(/\/whoowns (.+)/, function(msg, match) {
    if (refuse("whoowns", msg)) {
        return;
    }
    /*
        if(msg.from.username !== "Lignite"){
            bot.sendMessage(msg.chat.id, "I'm sorry Mr. Ball, I'm afraid I can't let you do that.", {reply_to_message_id:msg.message_id});
            return;
        }
        */
    fs.readFile("resources/thoughts.json", 'utf8', function(err, data) {
        if (err) {
            console.error("Could not open file: %s", err);
            bot.sendMessage(msg.chat.id, "I had some trouble gathering my thoughts (" + err + ")", {
                reply_to_message_id: msg.message_id
            });
            return;
        }
        var jsonData = JSON.parse(data);
        var identity = match[1].toLowerCase();
        if (jsonData[identity] === undefined) {
            bot.sendMessage(msg.chat.id, "Nobody has created this thought yet :O", {
                reply_to_message_id: msg.message_id
            });
            return;
        }
        NSLog(JSON.stringify(jsonData) + " and " + jsonData[identity].ownerName);
        bot.sendMessage(msg.chat.id, "The owner of thought '" + identity + "' is " + jsonData[identity].ownerName + " (" + jsonData[identity].owner + ")", {
            reply_to_message_id: msg.message_id
        });
    });
});

bot.onText(/\/groupchat (.+)/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    bot.sendMessage(-12880731, match[1]);
});
bot.onText(/\/message (.+)/, function(msg, match) {
    if (msg.from.username !== "Lignite") {
        return;
    }
    bot.sendMessage(parseInt(match[1]), match[1].substring(match[1].indexOf(":") + 1));
});

bot.onText(/\/removethought (.+)/, function(msg, match) {
    if (refuse("removethought", msg)) {
        return;
    }

    currentlyTyping(msg);

    fs.readFile("resources/thoughts.json", 'utf8', function(err, data) {
        if (err) {
            console.error("Could not open file: %s", err);
            bot.sendMessage(msg.chat.id, "I had some trouble gathering my thoughts (" + err + ")", {
                reply_to_message_id: msg.message_id
            });
            return;
        }

        var phrases = JSON.parse(data);
        var identity = match[1].toLowerCase();
        if (phrases[identity] !== undefined) {
            if (phrases[identity].owner === msg.from.id || (msg.from.username === "Lignite")) {
                NSLog("Is from the same owner");
            } else {
                NSLog("Is NOT from the same owner!!!");
                bot.sendMessage(msg.chat.id, "You didn't give me this thought so you can't delete it, sorry", {
                    reply_to_message_id: msg.message_id
                });
                return;
            }
        }

        phrases[identity] = undefined;

        fs.writeFile("resources/thoughts.json", JSON.stringify(phrases), function(err) {
            if (err) {
                console.error("Could not write file: %s", err);
            }
            bot.sendMessage(msg.chat.id, "Thought deleted from my brain.", {
                reply_to_message_id: msg.message_id
            });
        });
    });
});

function addThought(msg, match) {
    if (refuse("addthought", msg)) {
        return;
    }

    if (msg.chat.type !== "private") {
        bot.sendMessage(msg.chat.id, "Due to spamming and Dan whining, you can now only add thoughts in private chat: @WoodyardBot", {
            reply_to_message_id: msg.message_id
        });
        return;
    }

    currentlyTyping(msg);

    /*
    if(msg.chat.type === "group"){
        bot.sendMessage(msg.chat.id, "Sorry, you can't add thoughts through group chats. Please message me privately to add thoughts.", { reply_to_message_id: msg.message_id });
        return;
    }
    */

    fs.readFile("resources/thoughts.json", 'utf8', function(err, data) {
        if (err) {
            console.error("Could not open file: %s", err);
            bot.sendMessage(msg.chat.id, "I had some trouble gathering my thoughts (" + err + ")", {
                reply_to_message_id: msg.message_id
            });
            return;
        }

        var identity = match[1].toLowerCase();

        if (!identity.contains(": ")) {
            bot.sendMessage(msg.chat.id, "Sorry dawg, I can't add that thought. Please enter it in the correct format, for example:\n\n/addthought jaldeep: always lost", {
                reply_to_message_id: msg.message_id
            });
            return;
        }

        var firstColon = identity.indexOf(":");
        var name = identity.substring(0, firstColon);
        var phraseString = match[1].substring(firstColon + 2);
        NSLog("Name " + name + " and phrase " + phraseString);

        var phrases = JSON.parse(data);

        if (phrases[name] !== undefined) {
            if (phrases[name].owner === msg.from.id) {
                NSLog("Is from the same owner");
            } else {
                NSLog("Is NOT from the same owner!!!");
                bot.sendMessage(msg.chat.id, "You didn't give me this thought so you can't change it, sorry", {
                    reply_to_message_id: msg.message_id
                });
                return;
            }
        }

        phrases[name] = {
            phrase: phraseString,
            owner: msg.from.id,
            ownerName: msg.from.first_name,
            timeAdded: msg.date
        };

        fs.writeFile("resources/thoughts.json", JSON.stringify(phrases), function(err) {
            if (err) {
                console.error("Could not write file: %s", err);
            }
            bot.sendMessage(msg.chat.id, "Thought engraved in my brain.", {
                reply_to_message_id: msg.message_id
            });
        });
    });
}

bot.onText(/\/at (.+)/, function(msg, match) {
    addThought(msg, match);
});

bot.onText(/\/addthought (.+)/, function(msg, match) {
    addThought(msg, match);
});

var NOT_SURE_MESSAGES = [
    "You know, I'm not really sure.", "They're pretty stupid", "What comes out of the south end of a north bound cow?",
    "Stop", "Jingle bell, Jingle bell", "Why the fuck do you want to know", "8/10 would bang again", "Part of the crew",
    "All good in my books, bad in my heart", "g/2", "2/g", "Not that hardcore of a memer", "Dropped out for a reason",
    "Don't trigger me on this nice day"
];

function thoughtsOn(msg, match) {
    if (refuse("thoughtson", msg)) {
        return;
    }

    currentlyTyping(msg);

    fs.readFile("resources/thoughts.json", 'utf8', function(err, data) {
        var phrases = JSON.parse(data);
        setTimeout(function() {
            var identity = match[1].toLowerCase();
            if ((phrases[identity] != undefined)) {
                //NSLog("Sending FUCKING SHIT");
                bot.sendMessage(msg.chat.id, phrases[identity].phrase, {
                    reply_to_message_id: msg.message_id
                });
            } else {
                NSLog("Sending backup");
                var toSend = NOT_SURE_MESSAGES[Math.floor(Math.random() * NOT_SURE_MESSAGES.length)];
                bot.sendMessage(msg.chat.id, toSend, {
                    reply_to_message_id: msg.message_id
                });
            }
        }, 1000);
    });
}

bot.onText(/\/thoughton/, function(msg, match) {
    thoughtsOn(msg, match);
});

bot.onText(/\/thoughtson (.+)/, function(msg, match) {
    thoughtsOn(msg, match);
});

bot.onText(/\/to (.+)/, function(msg, match) {
    thoughtsOn(msg, match);
});
